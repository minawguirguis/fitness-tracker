import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';

import { User } from './user.model';
import { AuthData } from './auth-data.model';
import {AngularFireAuth} from '@angular/fire/auth';
import {TrainingService} from '../training/training.service';
import {MatSnackBar} from '@angular/material';
import {UIService} from '../shared/ui.service';

@Injectable()
export class AuthService {
  authChange = new Subject<boolean>();
  private user: User;
  private isAuthenticated = false;

  constructor(private router: Router,
              private afAuth: AngularFireAuth,
              private  trainingService: TrainingService,
              private snackBar: MatSnackBar,
              private uiService: UIService) {}

  initAuthListner() {
    this.afAuth.authState.subscribe( user => {
      if(user){
        this.isAuthenticated = true;
        this.authChange.next(true);
        this.router.navigate(['/training']);
      } else {
        this.trainingService.cancelSubscription();
        this.authChange.next(false);
        this.router.navigate(['/login']);
        this.isAuthenticated = false;
      }
    })

  }

  registerUser(authData: AuthData) {
    this.uiService.loadingStateChange.next(true);
    this.afAuth.auth.createUserWithEmailAndPassword(authData.email, authData.password).then(result => {
      this.uiService.loadingStateChange.next(false);
    })
    .catch(error => {
      this.uiService.loadingStateChange.next(false);
      this.uiService.showSnackBar(error.message, null, 3000);
      // this.snackBar.open(error.message, null, {duration: 3000});
    });
  }

  login(authData: AuthData) {
    this.uiService.loadingStateChange.next(true);
    this.afAuth.auth.signInWithEmailAndPassword(authData.email, authData.password).then(result => {

      this.uiService.loadingStateChange.next(false);
    })
    .catch(error => {
      this.uiService.loadingStateChange.next(false);
      this.uiService.showSnackBar(error.message, null, 3000);
      // this.snackBar.open(error.message, null, {duration: 3000});
    });
  }

  logout() {
    this.afAuth.auth.signOut();

  }



  isAuth() {
    return this.isAuthenticated;
  }

}
