// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA_KWpso040xg_WSqNu-MPKSycKM5h_kXA',
    authDomain: 'ng-fitness-tracker-6b21a.firebaseapp.com',
    databaseURL: 'https://ng-fitness-tracker-6b21a.firebaseio.com',
    projectId: 'ng-fitness-tracker-6b21a',
    storageBucket: 'ng-fitness-tracker-6b21a.appspot.com',
    messagingSenderId: '446354774713',
    appId: '1:446354774713:web:33606c90760c8be4'
  }
};
